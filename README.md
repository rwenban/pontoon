# Factornine - Pontoon

Factornine's 'Pontoon' is a two-player game using the [Shephertz](http://www.shephertz.com) Cloud Ecosystem and written using HTML5, CSS and JavaScript.

## Game Overview

The game is a version of the simple card game 'Pontoon' ( or 'Blackjack' ), to be played by two players over a network.

## The Games Purpose

This project will provide me with a working knowledge of both the App42 API and of how to make a complex Angular application.

## Iterative stages

1. Introductory Stage
	
2. Card Game Design
	
3. Implement basic flow and design.
	
4. Implement basic two player game.

5. Make more extensive gameplay.

    * Implement the complex rules.
	*	Do some fantastic interactive animations and graphics.
	
6. Suss out Grunt

    * learn how to use components
	* learn how to set up bootstrap 3.0
	* learn how to minify
	* check the LINT errors
	
	
7. Catch all errors in the application!

8. Use angular services for various shared tasks

9. Gain a better understanding of Git 
	for example - use the ignore file
	
10. Wicked Animations

    1.	See: http://docs.angularjs.org/api/ng.directive:ngShow for animations CSS