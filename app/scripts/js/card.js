'use strict';

// the reusable card object
// declare 'Card' before assignment to remove jshint warning
var Card;
Card = {
	// the constructor
	init: function (value, suit) {
		this.assignStatics();
		this.value = value;
		this.suit = suit;
		return this;
	},
	assignStatics: function () {
		this.CLUBS = 0;
		this.SPADES = 1; // Codes for the 4 suits, plus Joker.
		this.HEARTS = 2;
		this.DIAMONDS = 3;
		this.JOKER = 4;
		this.ACE = 1; // Codes for the non-numeric cards.
		this.JACK = 11; //   Cards 2 through 10 have their
		this.QUEEN = 12; //   numerical values for their codes.
		this.KING = 13;
	},
	getSuitAsString: function () {
		switch (this.suit) {
		case this.SPADES:
			return 'Spades';
		case this.HEARTS:
			return 'Hearts';
		case this.DIAMONDS:
			return 'Diamonds';
		case this.CLUBS:
			return 'Clubs';
		default:
			return 'Joker';
		}
	},
	getValueAsString: function () {
		if (this.suit === this.JOKER) {
			return '' + this.value;
		} else {
			switch (this.value) {
			case 1:
				return 'Ace';
			case 2:
				return '2';
			case 3:
				return '3';
			case 4:
				return '4';
			case 5:
				return '5';
			case 6:
				return '6';
			case 7:
				return '7';
			case 8:
				return '8';
			case 9:
				return '9';
			case 10:
				return '10';
			case 11:
				return 'Jack';
			case 12:
				return 'Queen';
			default:
				return 'King';
			}
		}
	},
	// used to find the order for the card images online
	// in order: clubs, spades, hearts, diamonds
	// will be value * suit
	getOrdinalNumber: function () {
		console.log('card::getOrdinalNumber || value: ' + this.value + ' | suit: ' +this.suit );
		// the suit is zero indexed
		// also, presently the cards are in descending order with aces high!
		// 1 aces
		if( this.value <= 1) {
			return this.value * ( this.suit + 1 );
		}
		// 2 we need to reverse 2 - 13
		// TODO explain this algorithim
		else{
			// the card value will be 1 (ace), 2 (king) etc 
			var cardValue = Math.abs( this.value - 15 );
			console.log('card::getOrdinalNumber || cardValue: ' + cardValue );
			var newValue = (4 * ( cardValue - 1 ) ) + ( this.suit + 1 ) ;
			return newValue;
		}
		
		//return this.value * ( this.suit + 1 );
	},
	toString: function () {
		if(this.suit === this.JOKER ){
			if( this.value === 1) {
				return 'Joker';
			} else {
				return 'Joker #' + this.value;
			}
		} else {
			return this.getValueAsString() + ' of ' + this.getSuitAsString();
		}
	},
	getNumericalValue: function () {
		if (this.suit === this.JOKER) {
			return this.value;
		}
		if ( this.value > 10 ) {
			return 10;
		}
		else {
			return this.value;
		}
	}
};