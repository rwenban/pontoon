'use strict';

/*global StateMachine */

app.factory('stateMachine', function( $rootScope ) {
	
	//-------------
	// Statics
	//-------------
	var ERROR = -1;
	
	var NONE = 0;
	
	var WAITING = 1;
	
	var READY = 2;
	
	var SHUFFLED = 3;
	
	var DEALING = 4;
	
	var EVALUATING = 5;
	
	var FINISHED = 6;
	
	// the scope reference to the finite state machine to be returned
	var fsm = {};
	
	// necessary to contain the event handlers
	var stateMachine = {};
	
	
	// define the API
	var api = {
		
		// create
		create : function () {
		
			fsm = StateMachine.create({
				events: [
					{ name: 'start', from: 'none', to: 'waiting'},
					{ name: 'makeReady', from: ['waiting','finished'], to: 'ready'},
					{ name: 'shuffle', from: 'ready', to: 'shuffled'},
					{ name: 'deal', from: ['shuffled', 'evaluating'], to: 'dealing'},
					{ name: 'evaluate', from: ['dealing','shuffled'], to: 'evaluating'},
					{ name: 'finishGame', from: ['playing','evaluating'], to: 'finished'}
				],
				callbacks: {
					onchangestate: function(event, from, to) {
							//stateMachine.progressEvent("CHANGED STATE: " + from + " to " + to);
							stateMachine.onChangedState( event, from, to );
						},
					onSTATE : function(event, from, to) {
							stateMachine.onSTATE( event, from, to);
						}
				}
			});
			
			//console.log("stateMachine::A new StateMachine has been created!")
			
			return fsm;
			
		},// end of Create
			
		NONE : function () {
			return NONE;
		},
		
		WAITING : function () {
			return WAITING;
		},
		
		READY : function () {
			return READY;
		},
		
		SHUFFLED : function () {
			return SHUFFLED;
		},
		
		DEALING : function () {
			return DEALING;
		},
		
		EVALUATING : function () {
			return EVALUATING;
		},
		
		FINISHED : function () {
			return FINISHED;
		},
		
		//-------------
		// FSM Methods
		//-------------
		//
		
		// returns the current state ( as an integer )
		current : function () {
			return fsm.current;
		},
		
		// simply retutns the current state as an integer
		currentStateAsInt : function () {
			
			if( fsm.current === 'none') {
				return NONE;
			}
			
			else if( fsm.current === 'waiting') {
				return WAITING;
			}
			
			else if( fsm.current === 'ready') {
				return READY;
			}
			
			else if( fsm.current === 'shuffled') {
				return SHUFFLED;
			}
			
			else if( fsm.current === 'dealing') {
				return DEALING;
			}
			
			else if( fsm.current === 'evaluating') {
				return EVALUATING;
			}
			
			else if( fsm.current === 'finished') {
				return FINISHED;
			}
			
			else {
				return ERROR;
			}
		},
		
		// tests if the supplied command cannot be called
		cannot : function ( method ) {
			return fsm.cannot( method );
		},
		
		can : function ( method ) {
			return fsm.can( method );
		},
		
		//return true if the supplied state is the current state
		is : function ( state ) {
			return fsm.is( state );
		},
		
		//-------------
		// commands
		//-------------
		//
		
		// a method to start the finite state machine
		start : function () {
			fsm.start();
		},
		
		// for when the first user enters the games room
		makeReady : function () {
			fsm.makeReady();
		},
		
		// get the cards ready
		shuffle : function () {
			fsm.shuffle();
		},
		
		deal : function () {
			fsm.deal();
		},
		
		evaluate : function () {
			fsm.evaluate();
		},
		
		// when two players are ready to play
		startGame : function () {
			fsm.startGame();
		},
		
		// when the game has completed
		finishGame: function () {
			fsm.finishGame();
		},
		
		// test
		hello : function () {
			return 'stateMachine::hello from the State Machine!';
		}
	}; // end of API
	
	
	//--------------------------
	// event dispatchers
	//--------------------------
	//

	// evoked on start
	stateMachine.startUp = function ( result ) {
		$rootScope.$broadcast('startUp',  result );
	};
	
	// the progress event
	stateMachine.progressEvent = function (  message ){
		$rootScope.$broadcast('onProgressEventHandler', message );
	};
	
	// evoked when the machine has completed the change
	stateMachine.onChangedState = function ( event, from, to ){
		console.log('stateMachine.onChangedState || from, to: ' , from, to );
		$rootScope.$broadcast('onChangedStateHandler', event, from, to );
	};
	
	stateMachine.onSTATE = function ( event, from, to ){
		$rootScope.$broadcast('onSTATEHandler', event, from, to );
	};
	
	// Return the API as our factory definition.
	return (api);
	
});