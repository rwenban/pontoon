'use strict';
// a deck class based on the java Deck class
// see: http://math.hws.edu/eck/cs124/javanotes6/source/Deck.java
app.factory('deckService', function( ) {
	
	/**
    * An array of 52 or 54 cards.  A 54-card deck contains two Jokers,
    * in addition to the 52 cards of a regular poker deck.
    */
	var deck = [];
	
	var service = {};
   
   /**
    * Keeps track of the number of cards that have been dealt from
    * the deck so far.
    */
	var cardsUsed;
   
/**
	* Constructs a regular 52-card poker deck.  Initially, the cards
	* are in a sorted order.  The shuffle() method can be called to
	* randomize the order.  (Note that "new Deck()" is equivalent
	* to "new Deck(false)".)
*/
	 
	 
	// define the API
	var api = {
		
		createDeck : function ( includeJokers ) {
			
			if( includeJokers ) {
				// doNothing(); // TODO - implement at a later date
			}
			
			var cardCt = 0, suit = 0; // How many cards have been created so far.
			
			/*global Card */
			for ( suit = 0; suit <= 3; suit++ ) {
				for ( var value = 1; value <= 13; value++ ) {
					var card = Object.create(Card).init(value, suit);
					deck[cardCt] = card;
					cardCt++;
				}
			}
			cardsUsed = 0;
			
			service.shuffle();
			
			return deck;
		},
		
		dealCard : function () {
			
			if (cardsUsed === deck.length){
				// TODO handle error
			}
			cardsUsed++;
			
			return deck[cardsUsed - 1];
		}
		
	}; // end of API
	
	/**
	* Put all the used cards back into the deck (if any), and
	* shuffle the deck into a random order.
	*/
	service.shuffle = function () {
		for ( var i = deck.length-1; i > 0; i-- ) {
			var rand = Math.floor(( Math.random() * (i+1) ));
			//int rand = (int)(Math.random()*(i+1));
			var temp = deck[i];
			deck[i] = deck[rand];
			deck[rand] = temp;
		}
		cardsUsed = 0;
	};
	
	// Return the API as our factory definition.
	return (api);
});