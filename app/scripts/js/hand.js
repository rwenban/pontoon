'use strict';

// the reusable hand object
// declare 'Hand' before assignment to remove jshint warning
var Hand;
Hand = {

	// the constructor
	init: function () {
		this.hand = [];
		return this;
	},
	addCard : function ( card ) {
		this.hand.push( card );
	},
	getCard : function ( position ) {
		if(position < 0 || position > ( this.hand.length -1 )){
			console.log('error');
			return '';
		}
		else {
			return this.hand.splice(position, 1);
		}
	},
	getHand : function () {
		return this.hand;
	}
};