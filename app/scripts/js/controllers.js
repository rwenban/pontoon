'use strict';


/*global Card, Hand */
/*global  App42, App42User, AppWarp*/

var apiKey = '766a04525c2fdaded2765ecd1c8168f4602ace45aa8dc61339ffa6c2c6263206';

var secreteKey = 'e247c89d564ca3867df11c411745fdfc19d60dc9fb1670dbca710c3a7c9ebf6a';
		   
// This object will be filled by the form
var user = {};

var roomId;

var loggedIn = false;

var userService;

var DATA_BASE = 'game_db';

var collectionName = 'pontoon_game';

// define the controllers
angular.module('myApp.controllers', [] )
    .controller('LoginCtrl', ['$scope', '$log', '$location', 'App42UserService', 'createApp42User', '$timeout', 'utilities', function( $scope, $log, $location, App42UserService, createApp42User, $timeout, utilities ) {
			
			//-------------------
			// The Login Controller
			//-------------------
			//

	// Initialize the Application  
	App42.initialize(apiKey, secreteKey);
	
	userService  = new App42User();
	
	$scope.message = 'Not logged in!';
	
	// a flag which 
	$scope.useNewMember = true;
	
	$scope.inRoom = false;
	
	// switch between existing and new member
	$scope.inputModel = 'existing_member';
	
	//TODO - check if we need this...
	$scope.$watch('inputModel', function( value ){
		$log.info('inputModel: ' ,value );
	});

	// evoked on form submission
	$scope.login = function () {
		 
		var username,
			password,
			emailId;
		
		if( $scope.user )
		{
			username = $scope.user.username;
			password = $scope.user.password;
			emailId =  $scope.user.email;
		}
		
		// either the user has chosen existing or new user!
		if( $scope.inputModel === 'existing_member' ){
			$scope.authenticate( username, password );
		} else {
			$scope.createUser( username, password, emailId );
		}
	};
	
	// the method to create a new App42 user
	$scope.createUser = function ( username, password, emailId ) {
		// TODO - tidy!
		// use the service now!
		createApp42User.createUser( username, password, emailId )
			.then( function ( result ){

				$log.info('result: ' + result );
				/*
					example of the success object:
					------------------------------
					userName = dolly
					email = dolly@wenban.net 
					createdOn = 2014-01-15T09:52:09.238Z
					sessionId = 6180708d-ea6e-4e16-9bed-6b1ac26fde7e 
				*/
				
				// the failure object
				var fail;
				//
				try{
					// this statement will convert a string containing JSON notation into an object
					fail = JSON.parse( result );
					
					if( fail.hasOwnProperty('app42Fault') ){
						
						$scope.failure( result );
						
						return;
					}
				}
				// this is really arse over tit!
				catch( error ) {
					$log.info('error: ' + error );
					
					utilities.showProperties( result );
					
					$scope.authenticate( username, password );
				}
			}, function ( error ) {
				
				$log.info('error: ' + error );
				
				$scope.failure( error );
			});
	};

	// the method to authenticate an existing App42 user
	// success will result in an object like:
	// object: {'app42':{'response':{'success':true,'users':{'user':{'userName':'russell','accountLocked':false,'sessionId':'b0400633-f9fd-4172-a2c7-5bb233398205'}}}}} 
	// a failure in authenticatiion:
	// error: {'app42Fault':{'httpErrorCode':404,'appErrorCode':2002,'message':'Not Found','details':'UserName/Password did not match. Authentication Failed.'}} 

	$scope.authenticate = function ( username, password ) {
		
		// if the promise returns it can either have a 'success' response or have an error code
		App42UserService.authenticate( username, password )
		    .then( function ( result ) {

				if( result.hasOwnProperty('app42') ) {
					$scope.handleLoginSuccess( result, username );
				} else if( result.hasOwnProperty('app42Fault')  ) {
					$timeout( function(  ) {
						
						$scope.message = result.app42Fault.details;
						
					}, 25 ); // added the delay of 25 as a reference
				}
				else
				{
					$timeout( function(  ) {
						
						$scope.message = 'Authentication error.';
					
						$log.error('error: ', result );
					});
				}
				
			}, function( error ) {
				// promise rejected, could log the error with: console.log('error', error);
				
				$timeout( function(  ) {
					
					$log.error('error: ', error );
					
					$scope.message = 'Authentication error.';
				});
			});
	};
	
	// handles the successful login, opens the introduction page
	$scope.handleLoginSuccess = function ( obj, username ) {
		
		$timeout( function(  ) {
			// anything you want can go here and will safely be run on the next digest.
			
			$location.path('/introduction');
			
			// set the root value of user
			user.username = username;
			loggedIn = true;
		}, 50 );
	};
	
	
	
	// a utility which returns if the login is for a new rather than an existing user
	$scope.forNewMember = function () {
		return $scope.inputModel !== 'existing_member';
	};
	
	// a utility which returns true if all of the required fields of the form have been completed
	$scope.isFormValid = function () {
		// TODO check if we can use camel case here and signup-form in the html
		return ( $scope.forNewMember() ? ( $scope.signupForm.$valid ) : ( $scope.signupForm.name.$valid && $scope.signupForm.password.$valid )  );
	};
	
	// TODO - turn into a Service!?
	// a method which receives the App42 errors
	// NOTE: only for app42Fault errors!
	$scope.failure = function ( json ) {
		
		var obj = JSON.parse( json );
		
		$log.error('The app has failed! | ' + json );
		
		$log.error('The app has failed! | ' + obj.app42Fault.details );
		
		$scope.message = obj.app42Fault.details;
	
	};
}])
    .controller('IntroCtrl', ['$scope', '$log', '$location', '$window', 'App42UserService', 'warpClientService', 'mathHelper', 'utilities', 'stateMachine', function( $scope, $log, $location, $window, App42UserService, warpClientService, mathHelper, utilities, stateMachine) {
	 
			// the Controller for the Introduction
		
		if ( !loggedIn ) {
			$location.path('/');
		}
		
		$scope.rooms = {};
		
		$scope.message = 'Warp not ready!';
		
		$scope.chats = [];
		
		$scope.inRoom = false;
		
		$scope.roomId = undefined;
		
		$scope.errorCode = '0';
		
		// required for the error handling
		$scope.status = 'Not sent';
		
		$scope.adminMode = false;
		
		$scope.users = [];
		
		$scope.currentUserIsHost = false;
		
		// a flag required by the FSM
		$scope.ready = false;
		
		// the current state of the FSM
		$scope.current = 'Dont know';
		
		$scope.roomFull = false;
	
		// initiate the client
		warpClientService.init( apiKey, secreteKey );
		
		// initiate the FSM
		$log.info( stateMachine.hello() );
		
		stateMachine.create();

		$scope.current =  stateMachine.current();
    
		$scope.ready = true;
		
		// ---------------
		// watch  methods
		// ---------------
		//
		
		// this watch is triggered, but not necessary
		$scope.$watch($scope.users, function() {
			//$log.info('users has been updated!');
		});
		
		// TODO jshint isn't happy with the parmaters = 'defined but never used'
		// handles the FSM state change
		$scope.$on('onChangedStateHandler', function( event, from, to ){
			$log.info( event, from, to );
			$scope.current =  stateMachine.current();
		});
		
		
		$scope.$on('onSTATEHandler', function( event, from, to ){
			
			$log.info('IntroCtrl::onSTATEHandler || event, from, to: ', event, from, to );
			// $scope.current =  stateMachine.current();
		});
	
		// ---------------
		// button actions
		// ---------------
		//
		
		/* Sends a chat message to the Warp Server
		 *
		 * @message - the message to send 
		 */
		$scope.sendChat = function ( message )
		{
			warpClientService.sendChat( message );
		};
	
		/*
		 * the request to join room
		 * @roomId the id of the room to join
		 */
		$scope.joinRoom = function ( roomId ) {
		
			warpClientService.joinRoom( roomId );
		};
	
		/**
		 * a request to play a game
		 */
		$scope.playGame = function ( ){
			
			$log.info('IntroCtrl::playGame | roomid: ' + $scope.roomId + ' | isInRoom: ' + $scope.inRoom );
			
			// a bit of a hack use the root var
			roomId = $scope.roomId;
			
			var msg = { action:'play', user:'{user.username}' };
		
			msg.user = user.username;
		
			warpClientService.sendChat(JSON.stringify(msg));
		};
		
		/**
		 * the request to play as a banker
		 */
		$scope.playAsBanker = function ( ){
		
			$log.info('IntroCtrl::playAsBanker() | roomid: ' + $scope.roomId );
			
			var msg = { action:'banker', user:'{user.username}' };
		
			msg.user = user.username;
		
			warpClientService.sendChat(JSON.stringify(msg));
			
			// a bit of a hack use the root var -TODO - remove
			roomId = $scope.roomId;
		};
		
		/*
		 * a request to leave the current room
		 */
		$scope.leaveRoom = function () {
			
			$log.info('IntroCtrl::leaveRoom || roomId: ' + $scope.roomId );
			
			warpClientService.leaveRoom( $scope.roomId );
		};
		
		$scope.deleteRoom = function ( id ) {
			
			$log.info('IntroCtrl::deleteRoom || id: ' + id );
			
			warpClientService.deleteRoom( id );
		};
		
		$scope.closeElement = function ( element ) {
			
			$log.info('Element to close: ' + element );
		};
		
		// ---------------
		// event handlers
		// ---------------
		//
		
		/**
		 * the handler for the WarpClient connection
		 */
		$scope.$on('handleConnection', function( args, result ) {
			$scope.status = 'Sent';
			
			if( result === AppWarp.ResultCode.Success) {
				$scope.message = 'Warp success!';
				
				$scope.$apply();
				
				warpClientService.getAllRooms();
				
			} else {
				$scope.failure( args, result );
			}
		});
		
		/**
		 * handleOnGetAllRoomsDone - the result of the call to getAllRooms
		 * makes a call to update the room data
		 */
		$scope.$on('handleOnGetAllRoomsDone', function( args, rooms ){
			
			if( rooms.res === AppWarp.ResultCode.Success) {
				
				$scope.message = 'The game room has been found.';
				
				$scope.$apply();
				
				//need to make another request to get the detailed information
				for(var i=0; i<rooms.getRoomIds().length; ++i) {
					warpClientService.getLiveRoomInfo( rooms.getRoomIds()[i] );
				}
			} else {
				$scope.failure( args, rooms.res );
			}
		});
		
		/**
		 * Handles the response from the live info request
		 * Updates the users array
		 * The id will be a unique number
		 * The name will be 'GamesRoom'
		 */
		$scope.$on('onGetLiveRoomInfoHandler', function(  args, eventObject ){
			
			if( eventObject.getResult() === AppWarp.ResultCode.Success) {
				var id = eventObject.getRoom().getRoomId();
				
				$log.info('A ||IntroCtrl::onGetLiveRoomInfoHandler || id: ' + id + ' | name: ' +   eventObject.getRoom().getName() );
				
				$scope.rooms[ id ] = { 'id':id, 'name': eventObject.getRoom().getName() };
				
				$scope.roomId = id;
				
				$scope.users = eventObject.getUsers() || [];
				
				if( $scope.users.length === 1  && stateMachine.can('start') ) {
					stateMachine.start();
				} else if( $scope.users.length === 2 && stateMachine.can('makeReady') ) {
					stateMachine.makeReady();
				}
			} else {
				$scope.failure( args, eventObject.res );
			}
			$scope.$apply();
		});
		
		// handles the event dispatched when a user has joined a room
		// simply subscribes to the room
		// TODO - It does not seem clear what is sbubscribing to the room! ie there is no user parameter
		$scope.$on('onJoinRoomDoneHandler', function( args, room ) {
		
			if( room.getResult() === AppWarp.ResultCode.Success ) {
				warpClientService.subscribeRoom( room.getRoomId()) ;
				
			} else {
				$scope.failure( args, room.getResult() );
			}
		});
		
		// a method to handle the event when a user has subscribed to a room
		// now will use this to start the game...
		$scope.$on('onSubscribeRoomDoneHandler', function( args, room ) {
			
			$log.info('IntroCtrl::onSubscribeRoomDoneHandler');
			
			if(room.getResult() === AppWarp.ResultCode.Success) {
				$scope.inRoom = true;
				
				$scope.roomId = room.getRoomId();
				
				$scope.inGamesRoom = ( room.getName() === 'GamesRoom' );
				
				$scope.message = 'Joined Room : ' + room.getName();
	
				// TODO - make service?
				var msg = { action:'subscribed', user:'{user.username}'};
			
				msg.user = user.username;
			
				warpClientService.sendChat(JSON.stringify(msg));

				warpClientService.getLiveRoomInfo( room.getRoomId()  );
							
				$scope.$apply();
			
			} else {
				$scope.failure( args, room.getResult() );
			}
		});
		
		// a method to handle the event dispatched when a user leaves a room
		$scope.$on('onLeaveRoomDoneHandler', function( args, room ) {
			
			$log.log('\n\nIntroCtrl::onLeaveRoomDoneHandler | result: ' + room.getResult() + ' | id: ' + room.getRoomId());
			
			if(room.getResult() === AppWarp.ResultCode.Success) {
				warpClientService.unsubscribeRoom( room.getRoomId() );
			}
		});
		
		// handles the unscubscription from a room
		$scope.$on('onUnsubscribeRoomDoneHandler', function( args, room ) {
			
			if(room.getResult() === AppWarp.ResultCode.Success) {
				$scope.inRoom = false;
				
				$scope.roomId = -1;
				
				$scope.inRoom = false;
				
				warpClientService.getAllRooms();
				
				$scope.message = 'You have successfully left the room.';
				
				$scope.$apply();
			}
		});
		
		
		// NOTE: - not being recieved!
		$scope.$on('onUserJoinedRoomHandler', function( event, username) {
		
			$log.info('++ IntroCtrl onUserJoinedRoom || username: ' + username );

			$scope.$apply();
		});
		
		// handles the receiving of the chat messages
		// locationId
		$scope.$on('onChatReceivedHandler', function ( args, message ) {
			$log.info('\n\nIntroCtrl:: A onChatReceived: || message: ' + message );
			
			utilities.showProperties( message );
			
			try {
				var json = JSON.parse( message.getChat());
				
				$log.info('A good json! ', json);
				// JSON Message
				if ( json.action ===  'subscribed' ){
					
					$log.info('B USER HAS SUBSCRIBED! room id:  '+ message.getLocId() );
					
					$log.info('C users:  '+ $scope.users.length );
					
					// need to start the game if there are now two users
					warpClientService.getLiveRoomInfo( message.getLocId() );
					
				} else if ( json.action === 'play' ) {
					$log.info('C PLAYER good json! ', json);
					 
					// action: 'play', user: 'grace'
					if( json.user === user.username ) {
						
						user.banker = false;
						
						user.player = true;
					}
					
					$location.path('/game');
					 
				} else if (  json.action === 'banker') {
					
					if( json.user === user.username ) {
						
						user.banker = true;
						
						user.player = false;
					}
					
					$location.path('/game');
				}
				// do game move!
				$log.info('IntroCtrl Z good json! ', json);
				
			} catch(e) {
				
				$log.info('IntroCtrl:: A invalid json', json);
				
				var fullMessage =  message.getSender() + ' : ' + message.getChat();
				
				$log.info('IntroCtrl:: B onChatReceived: || message: ' + fullMessage );
				
				$scope.chats.push( fullMessage );
			
			}
			$scope.$apply();
		});
		
		// onDeleteRoomDoneHandler
		$scope.$on('onDeleteRoomDoneHandler', function( args, room ) {
			$log.info('A || IntroCtrl::onDeleteRoomDoneHandler || result: ' + room.getResult() );
			if(room.getResult() === AppWarp.ResultCode.Success) {
				
				var id = room.getRoom().getRoomId();
			
				$log.info('B || IntroCtrl::onDeleteRoomDoneHandler || room id: ' + id );
				
				$scope.rooms[ id ] = null;
			}
		});
		
		
		// a generic failure handler for the WarpClient
		// TODO check type of object
		/**
		 * @result - the App42 error code
		 */
		$scope.failure = function ( object, result ) {
			
			$log.error('IntroCtrl::failure || The app has failed! | object: ' + object );
			
			$log.error('IntroCtrl::failure || The app has failed! | obj: ' + result );
			
			$log.error();
			
			$scope.errorCode = result;
			
			$scope.message = 'Warp Failure!';
			
			$scope.$apply();
		};
	  
		 /**
		 * To reload the route
		 * A bit brutal - hopefully thre is a better way of doing this!
		 * TODO - hack we need a way of knowing the state of the Warp Client!
		 */
		$scope.reset = function () {
			$log.info('IntroCtrl::reset');
			$window.location.reload();
		};
		
		// ---------------
		// utilities
		// ---------------
		//

        /**
         * A utility tests if the current room id corresponds to the supplied value
         * Note: the function is named 'isInRoom' to avoid confusion with the boolean 'inRoom'!
         * @param roomId the id of the room to test
         * @returns {boolean}
         */
		$scope.isInRoom = function ( roomId ) {
			return ( $scope.roomId && $scope.inRoom && roomId === $scope.roomId );
		};
		
		$scope.roomEmpty = function () {
			return $scope.users.length === 0;
		};
		
		$scope.roomHasPerson = function () {
			return $scope.users.length && $scope.users.length > 0;
		};
		
		$scope.numberOfRooms = function () {
			return utilities.countProperties( $scope.rooms );
		};
		
		// ---------------
		// Main method
		// ---------------
		// 

        /**
         * connect to the Warp Client
         */
		if( user && user.username ) {
			warpClientService.connect( user.username );
		}
		
	}])
	.controller('GameCtrl', ['$scope', '$log', '$location',  '$timeout', 'App42UserService', 'warpClientService', 'storageService', 'utilities', 'stateMachine', 'parseApp42Data', 'mathHelper', 'deckService', function( $scope, $log, $location, $timeout, App42UserService, warpClientService, storageService, utilities, stateMachine, parseApp42Data, mathHelper, deckService ) {
		
		$log.info('Hi from the GameCtrl | roomId: ' + roomId);
		
		//-------------------
		// THE GAME CONTROLLER
		//-------------------
			
		$scope.isTesting = false;
			
		$scope.currentUser = user.username;
			
		$scope.message = 'waiting...';
			
			// set up instructions
		$scope.showInstructions = false;
		
		$scope.room = null;
		
		$scope.readyToPlay = false;
		
		$scope.gameHosted = false;
		
		$scope.thisMoveIsOdd = false;
		
		$scope.isThisPlayersTurn = false;
		
		$scope.thisUserIsTheBanker = false;
		
		$scope.playersTurn = false;
		
		$scope.bankersTurn = false;
		
		$scope.currentPlayer = undefined;
		
		$scope.otherPlayer = undefined;
		
		$scope.chats = [];
		
		$scope.model = {};
		
		$scope.cards = ['',''];
		
		$scope.model.images = [];
		
		$scope.stuck = [false,false];
		
		$scope.gameOver = false;
		
		$scope.gameOverReason = '';
		
		$scope.winner = undefined;
		
		$scope.isUsersTurn = false;
		
		$scope.playersTurn = false;
		
		$scope.bankersTurn = false;
		
		// $scope.totalValue = 0;
		
		$scope.stickDisabled = true;
		
		$scope.twistDisabled = true;
		
		$scope.betDisabled = true;
		
		// test the state of the stateMachine...
		$scope.current =  stateMachine.current();
		
		$log.info('GameCtrl:: - the controller is initializing - stateMachine current state: ', $scope.current );
		
		//-------------------
		// SERVICE HANDLERS
		//-------------------
		//
		
		//-------------------
		// for the FSM
		//-------------------
		//
		
		/**
		 * handles thw FSM state change
		 * This event is fired after the FSM state has changed
		 */
		$scope.$on('onChangedStateHandler', function( event, from, to ){
			$scope.current =  stateMachine.current();
			
			$log.info( event, from, to );
			
			$log.info('B Game::onChangedStateHandler || new state: ', $scope.current );
			
			switch ( stateMachine.currentStateAsInt() )
			{
			case stateMachine.NONE() :
				break;
			case stateMachine.WAITING() :
				break;
			case stateMachine.READY() :
				$scope.doReady();
				break;
			case stateMachine.SHUFFLED() :
				$scope.doShuffled();
				break;
			case stateMachine.DEALING() :
				$scope.doDealing();
				break;
			case stateMachine.EVALUATING() :
				$scope.doEvaluating();
				break;
			case stateMachine.FINISHED() :
				$scope.doGameFinished();
				break;
			default:
				$log.info('\nGame::onChangedStateHandler | default ');
				break;
			}
		});
		
		//-------------------
		// Main method
		//-------------------
		//
		
		/**
		 * makes a call to initialize the game model
		 * the model will only be created if either the 'user.banker' or 'user.player' has been set
		 * js note: the properties will be 'undefined' untill set, so will not evaluate as 'truthy'
		 */
		$scope.initializePlayer = function (){
		
			if( stateMachine && stateMachine.can('start') ) {
				stateMachine.start();
			}
			if( stateMachine && stateMachine.can('makeReady') ) {
				stateMachine.makeReady();
			}
			if( user.banker ) {
				$scope.createModel( user.username, true );
			} else if( user.player ) {
				$scope.createModel( user.username, false );
			}
		};
		
		
		//-------------------
    // Model
    //-------------------
    // 
		
		/**
		 * Makes the initial call to the Storage Service
		 * ( will only be once called by one player - see note above )
		 * 
		* @username	- the global username property
		* @banker	- this boolean is set by the orginal call to start the game
		*/
		$scope.createModel = function ( username, banker ) {
			
			$log.info('\n---------\nA createModel | username: ' + username + ' | as banker: ' + banker );
		
			var obj = { };
			
			obj.players = ['',''];
			
			// let the player go first in the array always ( odd starts! )
			if( banker ) {
				obj.players[1] = username;
			}
			// the banker is even
			else {
				obj.players[0] = username;
			}

			var json = ( JSON.stringify(obj) );
			
			$log.info('json: ',json );
			
			storageService.insertJSONDocument(DATA_BASE, collectionName, json)
			    .then( function ( result ) {
					// TODO need to check errors here - {'app42Fault	
					
					if( result.hasOwnProperty('app42Fault') ){
					
						$scope.failure( result );
	
						return;
					}
					
					$log.info('storageService || success || ' + result );
					
					var msg = { action:'modelcreated' };
					
					msg.model = result;
					
					warpClientService.sendChat(JSON.stringify(msg));
				
				}, function( error ) {
					// promise rejected, could log the error with: console.log('error', error);
					$log.info('storageService || failure || ' + error );
				});
		};
		
		
		//-------------------
		// do methods ( evoked by state update )
		//-------------------
		//
		
		// ready
		$scope.doReady = function () {
			$log.info('GameCtrl::doReady()');
		};
		
		// shuffled
		$scope.doShuffled = function () {
			$log.info('GameCtrl::doShuffled');
			
			$scope.stickDisabled = true;
			
			$scope.twistDisabled = true;
			
			$scope.betDisabled = true;
			
			$scope.updateGameLogic();
			
			// simply start dealing if you are the banker
			if(!$scope.thisUserIsTheBanker) {
				
				var msg = {  action:'statetobeupdated', from:'shuffled', to:'dealing' };
	
				warpClientService.sendChat( msg );
			}
		
			$scope.$apply();
		};
		
		
		/**
		 * Responsible for getting a card from the deck and dealing to the requested player
		 * only the banker has a set of cards and has created the deck object
		 * ( threfore there was no reason for the pack to be sent to the stored object )
		 * @param index the array index of the player to deal the card to
		 */
		$scope.doDealing = function ( index ) {
			
			$log.info('GameCtrl::doDealing');
			
			$scope.updateGameLogic();
			
			if ( index === undefined ) {
				if($scope.playersTurn) {
					index = 0;
				} else {
					index = 1;
				}
			}

			// deal to the current player ( or dealer )
			if( $scope.thisUserIsTheBanker ) {
				// get the card from the deck
				var card = deckService.dealCard();
			
				$log.info('E GameCtrl::doDealing ( should only be done by the banker )|| card: ' + card );
			
				// send the 'doDealing' message
				var msg = { action:'dealcard',  index:index, card:card };
			
				warpClientService.sendChat(JSON.stringify(msg));
			}
		};
		
		/**
		 * Is responsible for evaluating the current 'game model' for the current player
		 * see: $scope.updateGameLogic too!
		 */
		$scope.doEvaluating = function () {
			
			$log.info('+++++++++++++++  GameCtrl::doEvaluating +++++++++++++++' );
			
			var msg;
			
			// do the math
			
			var index = $scope.thisMoveIsOdd ? 0 : 1;
			
			var hand = $scope.model.hands[ index ];
			
			
			
			// test type
			// $log.info('\t\t hand is a Hand: ' + Hand.isPrototypeOf($scope.model.hands[index]) );
			
			var cardObjects = hand.getHand();
			
			// var totalValues = []
			
			var totalValue = 0,
				value,
				isBusted = false,
				hasPontoon = false,
				aces = 0,
				card;
			
			for (var i = 0; i < cardObjects.length; i++) {
				$log.info(i + ' | ' + cardObjects[i]);
				card = cardObjects[i].toString();
				// $log.info(i + ' card -> ' + card);
				value = cardObjects[i].getNumericalValue();
				totalValue = totalValue + value;
				if( value === 1 ){
					aces++;
				}
			}
			
			var possibleValues = $scope.calculatePossibleValues();
			
			$log.info('GameCtrl::doEvaluating || possibleValues: ' + possibleValues );
			
			
			/// 
			
			$log.info('GameCtrl::doEvaluating || index: ', index );
			$log.info('GameCtrl::doEvaluating || totalValue: ', totalValue );
			
			// assume aces low for now
			//$scope.totalValue = totalValue;
			
			// TODO - tidy local vars
			
			$scope.model.totals[index] = totalValue;
			
			$log.info('GameCtrl::doEvaluating || totals || player 0 | ' + $scope.model.totals[0] + ' | player 1: ' + $scope.model.totals[1] );
			$log.info('GameCtrl::doEvaluating || stuck || player 0 | ' + $scope.stuck[0] + ' | player 1: ' + $scope.stuck[1] );
			
			if( totalValue > 21 ) {
				isBusted = true;
			} else if( totalValue === 21 ) {
				hasPontoon = true;
			}
			
			$scope.betDisabled = ( $scope.model.move > 2 );
			
			$scope.stickDisabled = ( $scope.model.move < 3 || isBusted || hasPontoon || $scope.stuck[index]===true);
			
			$scope.twistDisabled = ( $scope.model.move < 3 || isBusted || hasPontoon || $scope.stuck[index]===true );
			
			if( isBusted ) {
				msg = {  action:'statetobeupdated', from:'evaluating', to:'finished', reason:'busted'  };
				warpClientService.sendChat( msg );
			}
			
			else if ( hasPontoon ) {
				msg = {  action:'statetobeupdated', from:'evaluating', to:'finished', reason:'pontoon'  };
				warpClientService.sendChat( msg );
			}
			else if ( $scope.stuck[0]===true && $scope.stuck[1]===true ) {
				msg = {  action:'statetobeupdated', from:'evaluating', to:'finished', reason:'points'  };
				warpClientService.sendChat( msg );
			}
			else {
				$log.info('GameCtrl::doEvaluating || has something has gone wrong?');
			}
			
			
			
			
			$log.info('+++++++++++++++ * +++++++++++++++ * +++++++++++++++' );
		};
		
		/**
		 * responds to the ending of the game
		 */
		$scope.doGameFinished = function () {
			
			$log.info('GameCtrl::doGameFinished!');
			
			$scope.gameOver = true;
			
			$scope.message = 'Game Over!';
			
			$scope.updateGameLogic();
			
			$scope.$apply();
		};
		
		// ---------------
		// button actions
		// ---------------
		//
		/**
		 * Handles the request to send a chat to the other player
		 */
		$scope.sendChat = function ( message ){
			warpClientService.sendChat( message );
		};
		

		/**
		 * Handles the response from the 'bet' button
		 * see @betMadeHandler
		 */
		$scope.bet = function () {
			$log.info('GameCtrl::bet');
			
			// send bet event...
			var msg = { action:'betmade' };
			
			warpClientService.sendChat( JSON.stringify( msg ) );
		};
		
		/**
		 * Handles the response from the 'stick' button 
		 * Will only send a 'stickchosen' message request with the relevant id
		 */
		$scope.stick = function () {
			
			$log.info('\t\t GameCtrl:::stick || currentPlayer: ' + $scope.currentPlayer + ' | is sticking!');
			
			var index = $scope.thisMoveIsOdd ? 0 : 1;
						
			var msg = { action:'stickchosen', index:index };
			
			warpClientService.sendChat( JSON.stringify( msg ) );
		};
		
		/**
		 * Handles the response from the 'twist' button
		 */
		$scope.twist = function () {
			$log.info('\t\t GameCtrl::twist || currentPlayer: ' + $scope.currentPlayer + ' | is twisting!');
			
			// here we don't increment the move we are just send a request to deal!
			
			var msg = {  action:'statetobeupdated', from:'evaluating', to:'dealing' };
		
			warpClientService.sendChat( msg );
		};
		
		
		/**
		 * Handles the response from the 'close' buttons
		 */
		$scope.closeInstructions = function (){
			$scope.showInstructions = false;
		};
		
		/**
		* Handles the button action to re-start the game
		 * The action is 'playagain'
		 */
		$scope.playAgain = function () {
			
			$log.info('GameCtrl::playagain - should be sending message!');
			
			var msg = {  action:'playagain' };
		
			msg.user = user.username;
		
			warpClientService.sendChat( JSON.stringify( msg ) );
		};
		
		
    //-------------------
    // Message Handlers
    //-------------------
    // 
		
		/**
		 * Resonsible for handing the 'modelupdated' request
		 * Simply updates the current model with the new model supplied 
		 */
		// TODO see if the model is being passed
		$scope.modelUpdatedHandler = function ( model ) {
			$scope.model = model;
		};
		
		/**
		 * handles the 'statetobeupdated' request
		 * This is the command to change the state of the FSM
		 * @param message.from - the current state of the FSM
		 * @param message.to  - the state to transition to
		 * @param message.reason - an optional paramater
		 */
		$scope.stateToBeUpdatedHandler = function ( message ) {
			
			$log.info('+++++++ Game::stateToBeUpdatedHandler | message from: ' + message.from + ' | to: ' + message.to );
			
			var state = message.to;
			
			switch ( state  )
			{
			case 'shuffled' :
				{
					if( stateMachine.can('shuffle')  ){
						stateMachine.shuffle();
					}
					break;
				}
			case 'dealing' :
				{
					if( stateMachine.can('deal')  ){
						stateMachine.deal();
					}
					
					break;
				}
			case 'finished':
				{
					if( stateMachine.can('finishGame')  ){
						
						$scope.updateGameOverScreen( message );
						
						stateMachine.finishGame();
					}
					break;
				}
			default:
				{
					break;
				}
			}
		};
		
		/**
		 * Handles the model creation
		 * Evoked by the 'modelcreated' request, both players will receive this event
		 * the initiator of the game should not respond
		 * the non-initiator needs to send their details in the handshake!
		 * @param event.model - the new model
		 */
		$scope.modelCreatedHandler = function( event ) {
				
			$scope.model = parseApp42Data.createModel( event.model );
		
			$log.info('GameCtrl::modelCreatedHandler || id : ' , $scope.model.id );
			
			// in case the game is being replayed
			$scope.initializeModel();
			
			// hack to add the current user 
			// If the current user is not defined in the list of players
			// add it to the list and send the handshake with the new list!
			// TODO - ideally we would add to the actual model
			if(  $scope.model.players.indexOf( user.username ) < 0 ) {
				// find the index of the empty string	
				var index = $scope.model.players.indexOf('');
				
				$scope.model.players[index] = user.username;
				
				// create the message and send the handshake!	
				var msg = { action:'handshake' };
			
				msg.players = $scope.model.players;
			
				warpClientService.sendChat( JSON.stringify(msg) );
			}
		};
		
		/**
		 * Recieved so both players are aware of each other
		 * Evoked by the 'handshake' request
		 * The game is ready to start
		 * Only the banker deals with this method, creates the deck of cards
		 */
		$scope.handShakeReceivedHandler = function () {
			
			$log.info('-------------------------');
			$log.info('GameCtrl::handShakeReceivedHandler');
			
			$scope.updateGameLogic();
			
			
			if( stateMachine.can('shuffle') &&  $scope.thisUserIsTheBanker ) {
				$log.info('CUT THE DECK!! | SHOULD ONLY BE CALLED ONCE! | stateMachine: ' + stateMachine.current() );
				
				// set up the deck of cards 
				var deck = deckService.createDeck( false );
				
				// reconstruct the model with the new deck
				var obj = $scope.model;
			
				obj.deck = deck;
			
				var json = ( JSON.stringify(obj) );
				
				storageService.updateDocumentByDocId(DATA_BASE, collectionName, $scope.model.id, json )
				    .then( function ( result ) {
						$log.info('A || storageService || cutDeck || success || ' + result );
					
						if( result.hasOwnProperty('app42Fault') ){
							
							$scope.failure( result );
							
							return;
						}
					
						var msg = {  action:'statetobeupdated', from:'ready', to:'shuffled' };
						
						// NOTE: there is some issue with the message being too big!
						// but we can only assume that the storage data has been updated
						// therefore the deck data cannot be passed around but I assume it can be read
						warpClientService.sendChat( msg );
				
					}, function( error ) {
						// promise rejected, could log the error with: console.log('error', error);
						$log.info('storageService || cutDeck || failure || ' + error );
					});
			}
			
			$scope.message = 'Waiting to deal!';
		};


		/**
		 * The handler for the 'dealcard' action
		 * @param message.action the 'dealcard' action
		 * @param message.index the index within the array to be dealt
		 * @param message.card - the card to be dealt
		 */
		$scope.dealCardHandler = function ( message ) {
			
			$log.info('A GameCtrl::dealCardHandler || message: ' + message.action + ' | card value: ' + message.card.value + ' | card suit: '+ message.card.suit );
			
			// the prototype is lost so we have to create a new Card object
			var card = Object.create(Card).init( message.card.value, message.card.suit);
			
			//$log.info('B GameCtrl::dealCardHandler || card value ( 1-52 ): ' + card.getOrdinalNumber() );
			
			//$log.info('D GameCtrl::dealCardHandler have we a type: ' + Card.isPrototypeOf( card ) ) ;
			var image = 'http://www.factornine.co.uk/images/cards/' + card.getOrdinalNumber() + '.png';
			
			$scope.model.images[message.index].push( image );
			
			// and the new card to the players hand
			$scope.model.hands[ message.index].addCard( card );
			
			// we also need to populate the model.cards[index] array which just holds the names of the cards 
			$scope.model.cards[message.index].push( card.toString() );
			
			// update the users hands
			// TODO need to investigate a better way to do this
			$scope.getCardsInHand( 0 );
			
			$scope.getCardsInHand( 1 );
			
			// TODO - delay after animation
			// after dealing we can evalute the hands
			if ( stateMachine.can('evaluate') ) {
				stateMachine.evaluate();
			}
			
			$scope.$apply();
		};
		
		/**
		 * Handles the 'betmade' request
		 * @param message - contains the 'betmade' value
		 */
		$scope.betMadeHandler = function ( message ) {
			
			$log.info('A GameCtrl::betMadeHandler || a bet has been made! ', message );
			
			// here we simply increment the move and deal again...

			//TODO - disable button
			$timeout( function () {
				if( stateMachine.can('deal') ) {
					$scope.model.move = $scope.model.move +1;
					stateMachine.deal();
				}
			}, 200 );
		};
		
		/**
		 * Handles the request for a player to 'stick'
		 * @param message.action - the 'stickchosen' request
		 * @param message.index - the index of the player who wants to stick
		 */
		$scope.stickChosenHandler = function ( message ) {
			
			// 1 add the new stuck array!
			$log.info('A GameCtrl::stickChosenHandler || player to stick: ' + message.index );
			
			// simply add the true flag to the array 
			$scope.stuck[ message.index ] = true;
		
			// TODO - disable button
			$timeout( function () {
				
				// simply swap over to the other player ( no need for anything fancy! )
				$scope.model.move = $scope.model.move +1;
				
				$scope.updateGameLogic();
			
				$scope.doEvaluating();
				
			}, 200 );
		};
		
		/**
		 * Handles the request to play again
		 *
		 */
		$scope.playAgainHandler = function() {
			
			$log.info('A GameCtrl::playAgainHandler');
			
			//if(stateMachine.can('makeReady'))  stateMachine.makeReady();
			
			// TODO rename
			//$scope.handShakeReceivedHandler();
			$timeout( function () {
				
				
				$scope.resetProperties();
				$scope.initializeModel();
				$scope.model.images[0] = [];
				$scope.model.images[1] = [];
				
				if(stateMachine.can('makeReady')) {
					stateMachine.makeReady();
				}
				
				$scope.handShakeReceivedHandler();
				
				
			}, 200 );
			
			$log.info('A GameCtrl::playAgainHandler');
		};
		
    //-------------------
    // Game model
    //-------------------
    //
	
		/**
		 * updates all the game properties
		 */
		$scope.updateGameLogic = function () {
			
			$scope.thisMoveIsOdd = mathHelper.isOdd( $scope.model.move );
			
			$scope.thisUserIsTheBanker = $scope.model.players.indexOf( user.username ) === 1 ;
			
			$scope.playersTurn = $scope.thisMoveIsOdd;
		
			$scope.bankersTurn = !$scope.thisMoveIsOdd;
			
			if( $scope.thisMoveIsOdd ) {
				$scope.currentPlayer = $scope.model.players[0];
				$scope.otherPlayer = $scope.model.players[1];
			} else {
				$scope.currentPlayer = $scope.model.players[1];
				$scope.otherPlayer = $scope.model.players[0];
			}
			
			// if odd it's the players turn ( the second in the array  '1' )
			// if even it's the bankers turn ( the first in the array '0' ) - the 0 - 'parity of zero' zero is an even number!
			
			// 1 the user is a player and the move is odd
			$scope.isThisPlayersTurn = ( $scope.thisMoveIsOdd && !$scope.thisUserIsTheBanker ) || ( !$scope.thisMoveIsOdd && $scope.thisUserIsTheBanker );
			
			
			// evalute game over conditions
			if( $scope.gameOver ) {
				
				if( $scope.gameOverReason === 'pontoon') {
					$scope.winner = $scope.currentPlayer;
				}
				else if( $scope.gameOverReason === 'busted') {
					$scope.winner = $scope.otherPlayer;
				}
				else if( $scope.gameOverReason === 'points'){
					//
					//$log.info('HEY WE HAVE A POINTS WINNER!!');
					
					// the banker has a slight advantage
					var playersPoints = $scope.getTotalValue(1);
					
					var bankersPoints = $scope.getTotalValue(0);
					
					$log.info('GameCtrl::updateGameLogic || playersPoints: ' + playersPoints + ' | bankersPoints: ' + bankersPoints );
					
					if( playersPoints > bankersPoints){
						
						$log.info('player 1 won!');
						
						$scope.winner = $scope.model.players[0];
						
					} else {
						
						$log.info('banker 0 won!');
						
						$scope.winner = $scope.model.players[1];
					}
				}
			}
		};
		
		/**
		 * Resets the model data
		 */
		$scope.initializeModel = function () {
			
			$scope.model.move = 1;
			
			// add the additional properties here...
			$scope.model.hands = ['',''];
			
			
			$scope.model.images = [];
			
			$scope.model.images[0] = [];
			
			$scope.model.images[1] = [];
			
			// ths string representation of the cards[]
			$scope.model.cards = [[],[]];
			
			$scope.model.stuck = [false,false];
	
			$scope.model.hands[0] = Object.create(Hand).init();
			
			$scope.model.hands[1] = Object.create(Hand).init();
			
			$scope.model.totals = ['',''];
			
			$scope.model.totals[0] = 0;
			
			$scope.model.totals[1] = 0;
		};
		
		/**
		 * Resets the $scope properties to their default positions
		 */
		$scope.resetProperties = function () {
			
			$log.info('A GameCtrl::resetProperties');
			
			$scope.cards = ['',''];
		
			$scope.stuck = [false,false];
			
			// add the additional properties here...
			$scope.model.images = [];
			
			$log.info('A GameCtrl::resetProperties | top Len181: '+$scope.model.images.length );
			
			$scope.model.images[0] = [];
			
			$scope.model.images[1] = [];
			
			$log.info('B GameCtrl::resetProperties | new: '+$scope.model.images.length );
			
			$log.info('B GameCtrl::resetProperties | 0: '+$scope.model.images[0].length );
			
			$log.info('B GameCtrl::resetProperties | 1: '+$scope.model.images[1].length );
			
			$scope.model.hands = ['',''];
			
			$scope.model.hands[0] = Object.create(Hand).init();
			
			$scope.model.hands[1] = Object.create(Hand).init();
		
			$scope.gameOver = false;
		
			$scope.gameOverReason = '';
		
			$scope.winner = '';
		
			$scope.isUsersTurn = false;
		
			$scope.playersTurn = false;
		
			$scope.bankersTurn = false;
		
			// test the state of the stateMachine...
			$scope.current =  stateMachine.current();
			
			$scope.$apply();
		};

    //-------------------
    // Helper methods
    //-------------------
    //
		
		/**
		 * A helper function which returns the total value of cards
		 * @param player - the index of the player the whose card of hans we want to read
		 * TODO return a list of possible values to accommodate the ace being high or low
		 */
		$scope.getTotalValue = function ( player ){

			if( !$scope.model || !$scope.model.hands || $scope.model.hands[ player ] ) {
				return 0;
			}
				
			var hand = $scope.model.hands[ player];
			
			var cardObjects = hand.getHand();
			
			var totalValue = 0,
				string,
				aces = 0,
				value;
			
			for (var index = 0; index < cardObjects.length; index++) {
				string = cardObjects[index].toString();
				$log.info('E getTotalValue: string | ' + string);
				value = cardObjects[index].getNumericalValue();
				totalValue = totalValue + value;
				if( value === 1 ) {
					aces++;
				}
			}
					
			return totalValue;
		};
		
		
		/**
		 * Used to populate the array of cards for the specified user
		 * The information is a string describing the card
		 * @param user the user ( either index or name ) - use index for now...
		 */
		$scope.getCardsInHand = function ( user ) {

			if( !$scope.model.hands) {
				return;
			}
			
			//$log.info('B GameCtrl::getCardsInHand || user: ', user );
			
			// get the whole hand as an array ( of 'type' card )
			var hand = $scope.model.hands[ user ];
			
			// test
			//$log.info('D GameCtrl::getCardsInHand is the right type: ' + Hand.isPrototypeOf(hand) );
			
			var cardObjects = hand.getHand();
			
			//$log.info('E GameCtrl::getCardsInHand || obj: ' + cardObjects );
			
			// TODO - test this again - not sure if this is going to be the right scope...
			$scope.cards[user] = [];
			
			var string;
			// add to the simple array
			for (var index = 0; index < cardObjects.length; index++) {
				string = cardObjects[index].toString();
				$scope.cards[user].push( string );
			}
			
			$scope.$apply();
		};
		
		/**
		 * Responsible for calculating the possible values of the players hands
		 * Returns a two dimensional array
		 */
		$scope.calculatePossibleValues = function () {
			
			$log.info('A || GameCtrl::calculatePossibleValues');
			
			var hand,
				cardObjects,
				card,
				value,
				totalValue,
				possibleValues,
				aces;
				
			// the array to return
			var values = [];
				
			for(var i=0; i<2; i++) {
				
				totalValue = 0;
				
				possibleValues = [];
				
				aces = 0;
				
				hand = $scope.model.hands[ i ];
				
				cardObjects = hand.getHand();
					
				for (var j = 0; j < cardObjects.length; j++) {
					$log.info(j + ' | ' + cardObjects[j]);
					card = cardObjects[j].toString();
					$log.info(j + ' card -> ' + card);
					value = cardObjects[j].getNumericalValue();
					$log.info(j + ' value -> ' + value );
					totalValue = totalValue + value;
					if( value === 1 ) {
						aces++;
					}
					
					
				}
				possibleValues.push( totalValue  );
				
				// make an array of the possible values according to how many aces we have!
				for(var k = 0; k < aces; k++){
                    //possibleValues.push( totalValue + ( k+1 * 10 ));
                    // TODO test this works
					possibleValues.push( totalValue + ( k * 10 ));
				}
							
				// add to the values
				values.push( possibleValues );
			}
			
			$log.info('Z || GameCtrl::calculatePossibleValues || 0: ' + values[0] + " || 1: " + values[1] );
			
			return values;
		};
		

    //-------------------
    // UI View methods
    //-------------------
    //
		/**
		 * For the ui element which displays the gameover message
		 */
		$scope.updateGameOverScreen = function ( message ) {
			
			$log.info('GameCtrl::updateGameOverScreen | reason: ' + message.reason );
			
			$scope.gameOver = true;
			
			$scope.gameOverReason = message.reason;
			
			$scope.updateGameLogic();
			
			$scope.$apply();
		};
				

    //-------------------
    // warpClientService Handlers
    //-------------------
    //
		
		/**
		 * handles the receiving of the chat messages
		 * divides the messages into strings - for general chat, and json for game play
		 */
		$scope.$on('onChatReceivedHandler', function ( args, message ) {
			
			var fullMessage =  message.getSender() + ' : ' + message.getChat();
			
			var json,
				isValid = false;
			
			try{
				json = JSON.parse( message.getChat());
				isValid = true;
			}
			catch(error) {
				//$log.info('invalid json | error: ' + error);
				isValid = false;
			}
			
			$log.info('B GameCtrl::onChatReceivedHandler | isValid JSON : ' + isValid);
			if( isValid ) {
				
				if (json.action === 'modelcreated' ){
					
					$log.info('modelcreated | stateMachine: ' + stateMachine.current() );
					
					$timeout( function () {
						
						$scope.modelCreatedHandler( json );
					
					}, 100 );
					
				} else if (json.action === 'handshake'){
					
					$scope.model.players = json.players;
					
					$timeout( function () {
						
						$scope.handShakeReceivedHandler();
					
					}, 300 );
				
				} else if (json.action === 'modelupdated') {
				
					$scope.modelUpdatedHandler( json.model );
					
				} else if (json.action === 'statetobeupdated') {
					
					$scope.stateToBeUpdatedHandler( json );
					
				} else if (json.action === 'dealcard') {
					
					$scope.dealCardHandler( json );
				} else if ( json.action === 'betmade') {
					
					$scope.betMadeHandler( json );
				}
				else if ( json.action === 'stickchosen') {
					
					$scope.stickChosenHandler( json );
				}
				else if ( json.action === 'playagain') {
					
					$scope.playAgainHandler( json );
				}
			}
			
			// for invalid json that should be a normal chat message...
			else {
				
				if( message.getChat().indexOf('{') === 0 ) {
					$log.info('might have tried to send a json rather than a chat!');
				} else {
					// do normal chat
					$scope.chats.push( fullMessage );
				}
			}
			$scope.$apply();
		});
		
		
		/**
		 * live info request
		 */
		$scope.$on('onGetLiveRoomInfoHandler', function(  args, room ){
	
			if( !$scope.room &&  room.getResult() === AppWarp.ResultCode.Success && room.getRoom() ){
				
				$log.info('GameCtrl::OK - room is null! || new room: ' + room.getRoom().getName() );
				
				$scope.room = room.getRoom();
			}
			
			$log.info('A onGetLiveRoomInfoHandler');
					
			$scope.$apply();
		});
		
	
    /**
     * handles the event when a user has subscribed to a room
     */
		$scope.$on('onSubscribeRoomDoneHandler', function( args, room ) {
			
			$log.log('\n\nGameCtrl::onSubscribeRoomDoneHandler | result: ' + room.getResult() + ' | id: ' + room.getRoomId() );
			
			if( room.getResult() === AppWarp.ResultCode.Success ) {
				$scope.$apply();
			}
		});
		
		/**
		 * handles the event dispached when a user has joined a room
		 */
		$scope.$on('onJoinRoomDoneHandler', function( args, room ) {
			
			$log.log('GameCtrl::onJoinRoomDoneHandler | result: ' + room.getResult() + ' | id: ' + room.getRoomId() );
			
			if( room.getResult() === AppWarp.ResultCode.Success) {
				warpClientService.subscribeRoom( room.getRoomId() );
			}
		});
		
		/**
		 * handles the event dispached when a user has joined a room ?
		 * TODO - might be redundant
		 */
		$scope.$on('onUserJoinedRoomHandler', function( event, username) {
			
			$log.info('++ GameCtrl onUserJoinedRoom || username: ' + username );
			
			$scope.$apply();
		});
		
		
		/**
		 * Generic failure method
		 * TODO - might be redundant
		 */
		$scope.failure = function ( json ) {
			var obj = JSON.parse( json );
			$log.error('\n----------------\nThe app has failed! | ' + json );
			$log.error('\nThe app has failed! | ' + obj.app42Fault.details );
			$scope.message  = obj.app42Fault.details;
		};
		
		// ---------------
		// Main method
		// ---------------
		//
		
		$scope.initializePlayer();
	
	}]);