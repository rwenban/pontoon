'use strict';

//-------------
// Directives
//-------------
//

angular.module('myApp.directives', [])
	// a simple directive to display an error code
	.directive('errorCodeDisplay', function() {
		return {
			restrict: 'AE',
			require: '^errorCode',
			scope: {
				errorCode: '=',
				status: '=',
				methodToCall: '&method'
			},
			
			replace: true,
			
			template : '<p ng-include="getTemplateUrl()"></p>',
			
			controller: function ( $scope ) {
				// necessary as the ng-show requires a boolean value
				$scope.sent = function () {
					return ($scope.status === 'Sent') ? true : false;
				},
				$scope.getTemplateUrl = function() {
					return '/partials/errorCode.html';
				},
				$scope.reset = function () {
					$scope.methodToCall();
				}; // jshint ignore:line
			}
		};
	})
	// a directive which is responsible for displaying the players name and other information.
	.directive('playerDisplay', function() {
		return {
			restrict: 'AE',
			require: '^playerIndex',
			scope: {
				playerIndex: '=',
				players: '='
			},
			
			replace: true,
		
			template: '<p ng-include="getTemplateUrl()"></p>',
		
			controller: function ( $scope ) {
				$scope.getTemplateUrl = function() {
					return '/partials/playerDisplay.html';
				};
			}
		};
	})
	// card-animation-display
	.directive( 'cardAnimationDisplay', function() {
		return {
			restrict: 'AE',
			
			scope: {
				images: '='
			},
			
			replace: true,
			
			template: '<p ng-include="getTemplateUrl()"></p>',
			
			controller: function( $scope ) {
				$scope.getTemplateUrl = function() {
					return '/partials/cardAnimationDisplay.html';
				};
			}
		};
	})
	// for the controls for each player
	.directive( 'playerControlsDisplay', function( ) {
		return {
			restrict: 'AE',
			
			scope: {
				index: '=',
				total: '=',
				betDisabled: '=',
				stickDisabled: '=',
				twistDisabled: '=',
				betFn: '&',
				stickFn: '&',
				twistFn: '&'
			},
			
			replace: true,
			
			template: '<p ng-include="getTemplateUrl()"></p>',
			
			controller: function( $scope) {
				$scope.getTemplateUrl = function() {
					return '/partials/playerControlsDisplay.html';
				},
				$scope.bet = function( ) {
					console.log('bet' );
					$scope.betFn( );
				},
				$scope.stick = function( ) {
					console.log('stick: ' );
					$scope.stickFn( );
				},
				$scope.twist = function( ) {
					console.log('twist: ');
					$scope.twistFn( );
				}; // jshint ignore:line
			}
		};
	});