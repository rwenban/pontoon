
'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
// from: http://stackoverflow.com/questions/16709421/why-use-services-in-angular
/*global App42User, AppWarp  */
angular.module('myApp.services', []).
value('version', '0.1')
	.factory('App42UserService', function ( $q, $timeout ) {

		var getUserService = function () {

			var deferred = $q.defer();

			$timeout(function () {

				deferred.resolve(new App42User());

			}, 1000);
			return deferred.promise;
		};

		var getAllUsers = function () {
			return [];
		};

		var authenticate = function (username, password) {

			var deferred = $q.defer();

			var userService = userService;

			var userName = userName;

			new App42User().authenticate(username, password, {

				success: function (object) {

					var successObj = JSON.parse(object);

					deferred.resolve(successObj);
				},

				error: function (error) {
					// error will have 'app42Fault'
					var errorObj = JSON.parse(error);
					deferred.resolve(errorObj);
				}
			});
			return deferred.promise;
		};
		
		// the API
		return {
			getUserService: getUserService,
			
			getAllUsers: getAllUsers,
			
			authenticate: authenticate
		};
	})
	.factory('createApp42User', function ($q, $timeout, App42UserService) {

		var createUser = function (userName, password, emailId) {
			var deferred = $q.defer();

			var userService = userService;

			var email = emailId;

            // TODO tidy this userService reference!
			App42UserService.getUserService()
				.then(function (userService) {
					userService = userService;

					$timeout(function () {

						var result;

						userService.createUser(userName, password, email, {

							success: function (object) {

								var userObj = JSON.parse(object);

								result = userObj.app42.response.users.user;

								deferred.resolve(result);

							},
							error: function (error) {
								deferred.resolve(error);
							}
						});
					}, 50);
				}, function (json) {
					deferred.resolve(json);
				});
			return deferred.promise;
		};

		return {
			createUser: createUser
		};
	})
	.factory('warpClientService', function ($rootScope) {

		var warpclient = AppWarp.WarpClient.getInstance();

		var warpClientService = {};

		var api = {

			init: function (apiKey, secreteKey) {

				AppWarp.WarpClient.initialize(apiKey, secreteKey);

				//AppWarp.WarpClient.initialize( apiKey, 'dummy' ); // add dummy value to cause error

				warpclient.setResponseListener(AppWarp.Events.onConnectDone, warpClientService.onConnect);

				warpclient.setResponseListener(AppWarp.Events.onGetAllRoomsDone, warpClientService.onGetAllRoomsDone);

				warpclient.setResponseListener(AppWarp.Events.onGetLiveRoomInfoDone, warpClientService.onGetLiveRoomInfo);

				warpclient.setResponseListener(AppWarp.Events.onJoinRoomDone, warpClientService.onJoinRoomDone);

				warpclient.setResponseListener(AppWarp.Events.onSubscribeRoomDone, warpClientService.onSubscribeRoomDone);

				warpclient.setResponseListener(AppWarp.Events.onLeaveRoomDone, warpClientService.onLeaveRoomDone);

				warpclient.setResponseListener(AppWarp.Events.onUnsubscribeRoomDone, warpClientService.onUnsubscribeRoomDone);
				
				warpclient.setNotifyListener(AppWarp.Events.onChatReceived, warpClientService.onChatReceived);

				warpclient.setResponseListener(AppWarp.Events.onUserJoinedRoom, warpClientService.onUserJoinedRoom);

				warpclient.setResponseListener(AppWarp.Events.onSendMoveDone, warpClientService.onSendMoveDone);

				warpclient.setResponseListener(AppWarp.Events.onCreateRoomDone, warpClientService.onCreateRoomDone);

				warpclient.setResponseListener(AppWarp.Events.onDeleteRoomDone, warpClientService.onDeleteRoomDone);

			},

			connect: function (userName) {
				warpclient.connect(userName);
			},

			getAllRooms: function () {
				warpclient.getAllRooms();
			},

			getLiveRoomInfo: function (id) {
				warpclient.getLiveRoomInfo(id);
			},

			joinRoom: function (id) {
				warpclient.joinRoom(id);
			},

			subscribeRoom: function (id) {
				warpclient.subscribeRoom(id);
			},

			leaveRoom: function (id) {
				warpclient.leaveRoom(id);
			},

			unsubscribeRoom: function (id) {
				warpclient.unsubscribeRoom(id);
			},

			sendChat: function (message) {
				warpclient.sendChat(message);
			},

			createTurnRoom: function (name, owner, maxUsers, tableProperties, turnTime) {
				warpclient.createTurnRoom(name, owner, maxUsers, tableProperties, turnTime);
			},

			deleteRoom: function (id) {
				warpclient.deleteRoom(id);
			}
		};

		warpClientService.onConnect = function (result) {
			$rootScope.$broadcast('handleConnection', result);
		};

		warpClientService.onGetAllRoomsDone = function (result) {
			$rootScope.$broadcast('handleOnGetAllRoomsDone', result);
		};

		warpClientService.onGetLiveRoomInfo = function (result) {
			$rootScope.$broadcast('onGetLiveRoomInfoHandler', result);
		};

		warpClientService.onJoinRoomDone = function (result) {
			$rootScope.$broadcast('onJoinRoomDoneHandler', result);
		};

		warpClientService.onSubscribeRoomDone = function (result) {
			$rootScope.$broadcast('onSubscribeRoomDoneHandler', result);
		};

		warpClientService.onLeaveRoomDone = function (result) {
			$rootScope.$broadcast('onLeaveRoomDoneHandler', result);
		};

		warpClientService.onUnsubscribeRoomDone = function (result) {
			$rootScope.$broadcast('onUnsubscribeRoomDoneHandler', result);
		};

		warpClientService.onChatReceived = function (result) {
			$rootScope.$broadcast('onChatReceivedHandler', result);
		};

		warpClientService.onUserJoinedRoom = function (result) {

			$rootScope.$broadcast('onUserJoinedRoomHandler', result);
		};

		warpClientService.onSendMoveDone = function (result) {
			$rootScope.$broadcast('onSendMoveDoneHandler', result);
		};

		warpClientService.onCreateRoomDone = function (result) {

			$rootScope.$broadcast('onCreateRoomDoneHandler', result);
		};

		warpClientService.onDeleteRoomDone = function (result) {

			$rootScope.$broadcast('onDeleteRoomDoneHandler', result);
		};

		// Return the API as our factory definition.
		return (api);
	})
	.factory('storageService', function ( $q ) {
		/*global App42Storage*/
		var app42Storage = new App42Storage();

		var api = {

			insertJSONDocument: function (dbName, collectionName, json) {

				var deferred = $q.defer();

				console.log('storageService::insertJSONDocument');

				app42Storage.insertJSONDocument(dbName, collectionName, json, {

					success: function (result) {
						// Storage Object creation
						deferred.resolve(result);
					},
					error: function (error) {
						// Storage exception handled
						deferred.resolve(error);
					}
				});
				return deferred.promise;
			},

			findAllDocumentsCount: function (dbName, collectionName) {

				var deferred = $q.defer();

				app42Storage.findAllDocumentsCount(dbName, collectionName, {

					success: function (result) {

						deferred.resolve(result);

					},
					error: function (error) {
						deferred.resolve(error);
					}
				});
				return deferred.promise;
			},

			deleteAllDocuments: function (dbName, collectionName) {

				var deferred = $q.defer();

				app42Storage.deleteAllDocuments(dbName, collectionName, {

					success: function (result) {
						deferred.resolve(result);
					},

					error: function (error) {
						deferred.resolve(error);
					}
				});

				return deferred.promise;
			},

			updateDocumentByKeyValue: function (dbName, collectionName, key, value, newJsonDoc) {
				
				var deferred = $q.defer();

				app42Storage.updateDocumentByKeyValue(dbName, collectionName, key, value, newJsonDoc, {

					success: function (result) {
						deferred.resolve(result);
					},

					error: function (error) {
						deferred.resolve(error);
					}
				});
				return deferred.promise;
			},

			findDocumentById: function (dbName, collectionName, docId) {

				var deferred = $q.defer();

				app42Storage.findDocumentById(dbName, collectionName, docId, {

					success: function (result) {
						deferred.resolve(result);
					},

					error: function (error) {
						deferred.resolve(error);
					}
				});

				return deferred.promise;
			},

			findDocumentByKeyValue: function (dbName, collectionName, key, value) {

				var deferred = $q.defer();

				app42Storage.findDocumentByKeyValue(dbName, collectionName, key, value, {

					success: function (result) {
						deferred.resolve(result);
					},

					error: function (error) {
						deferred.resolve(error);
					}
				});

				return deferred.promise;
			},

			updateDocumentByDocId: function (dbName, collectionName, docId, newJsonDoc) {

				var deferred = $q.defer();

				app42Storage.updateDocumentByDocId(dbName, collectionName, docId, newJsonDoc, {

					success: function (result) {
						deferred.resolve(result);
					},

					error: function (error) {
						deferred.resolve(error);
					}

				});

				return deferred.promise;
			}
		};
		return api;
	})
// A utility for general maths functions
.factory('mathHelper', function ( ) {

	var api = {
		// returns an integer from 0 to the specified range 
		// if the range parameter is not set, the the number within the range 0-9
		randomNumber: function (range) {

			range = range || 9;

			return Math.floor((Math.random() * range));
		},
		// TODO sort jshint warning - 'Confusing use of '!' - see: http://stackoverflow.com/questions/7972446/javascript-not-in-operator-for-checking-object-properties
		isOdd: function (number) {
			if( (number % 2) === 0) {
				return false;
			} else {
				return true;
			}
		},

		isEven: function (number) {
			return (number % 2) === 0;
		}
	};
	return api;

})
	.factory('utilities', function () {

		var api = {

			// a utility which trace all the properties of the supplied object
			showProperties: function (obj) {
				console.log('---------');
				var prop;
				for (prop in obj) {
					console.log(prop + ' = ' + obj[prop]);
				}
				console.log('---------');
			},

			countProperties: function (obj) {
				var returnValue = 0,
					prop;
				for ( prop in obj ) {
					returnValue++;
				}
				return returnValue;
			}
		};

		return api;
	})
	.factory('parseApp42Data', function () {

		var data = {};

		var api = {
			// the data is in the form: model.app42.response.storage.jsonDoc._id.$oid
			createModel: function (json) {

				var model = JSON.parse(json);

				var source = model.app42.response.storage.jsonDoc;

				data.id = source._id.$oid;

				data.players = source.players;

				data.owner = source.owner;

				data.move = source.move;

				data.deck = source.deck;

				data.maximumMoves = source.maximumMoves;

				return data;
			}
		};
		return api;
	}) // the animation service
.animation('.slide-animation', function () {
	// we need the $scope!
	var getScope = function (e) {
		return angular.element(e).scope();
	};

	return {
		enter: function (element, done) {
			var $scope = getScope(element);
			/*global TweenMax */
			// TODO - adding the angle and the rotation makes a nice 'fan'
			//var angle = (1 * Math.PI * 0.5) - Math.PI / 4;
			// make the card visible
			TweenMax.to(element, 0.05, {
				opacity: 1,
				overwrite: 'none'
			});
			// move to the top position
			TweenMax.to(element, 0.55, {
				delay: 0.05,
				top: 10,
				//rotation: angle * ( $scope.cardNumber - 2 ),
				overwrite: 'none'
			});
			// move along
			TweenMax.to(element, 0.10, {
				delay: 0.60,
				left: element.prop('offsetLeft') + ($scope.$index * 25),
				overwrite: 'none',
				onComplete: done
			});
			done();
		},
		// will reset the element
		leave: function (element, done) {
			// TODO - The element is not being fully removed from the DOM so we have to set the element to be invisible.
			TweenMax.to(element, 0.05, {
				opacity: 0,
				overwrite: 'none',
				onComplete: done
			});
			done();
		}
	};
});