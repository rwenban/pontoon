'use strict';

// declare 'app' before assignment to remove jshint warning
var app;
app =  angular.module('pontoonApp', [
	'ngRoute',
	'myApp.services',
	'myApp.filters',
	'myApp.controllers',
	'myApp.directives',
	'ngCookies',
	'ngResource',
	'ngSanitize',
	'ngAnimate'
]).
config(['$routeProvider', function( $routeProvider ) {
	//$locationProvider.html5Mode(true);
	$routeProvider.when('/', {templateUrl: 'partials/login.html', controller: 'LoginCtrl'});
	$routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: 'LoginCtrl'});
	$routeProvider.when('/introduction', {templateUrl: 'partials/introduction.html', controller: 'IntroCtrl'});
	$routeProvider.when('/view1', {templateUrl: 'partials/partial1.html', controller: 'MyCtrl1'});
	$routeProvider.when('/test', {templateUrl: 'partials/test.html', controller: 'TestCtrl'});
	$routeProvider.when('/view2', {templateUrl: 'partials/partial2.html', controller: 'MyCtrl2'});
	$routeProvider.when('/game', {templateUrl: 'partials/game.html', controller: 'GameCtrl'});
	$routeProvider.otherwise({redirectTo: '/'});
}]);